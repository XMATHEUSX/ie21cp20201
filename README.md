# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Matheus H. Xavier|@XMATHEUSx|
|João Pedro Felicio Prudencio |@joao-pedro-prudencio|
|Emanuel Bebber |@EmanuelBebber|
|Gabriel Daleaste |@GabrielDaleaste |

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  https://xmatheusx.gitlab.io/ie21cp20201/

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
* [Link do Projeto no Tinkercad](https://www.tinkercad.com/things/4s9N2zFw6o7)

