# Documentação do Sensor De Estacionamento

## 1. Introdução

A implementação do projeto pode ser vista pela foto abaixo:

![](Projeto_final.png)


## 2. Objetivos

O aparelho visa, através do sensor de distância, informar ao motorista o quão próximo ele está de bater o carro. 

## 3. Materiais utilizados

### Lista de Materiais

| Componente | Imagem | 
|-----|-----|
| Arduino R3 ONU | ![](ArduinoUno.jpg) |
| LCD 16x2 | ![](PainelLed.jpg) |
| Protoboard pequena | ![](PlacaDeEnsaioPequena.jpg) |
| Sensor Ultrassônico | ![](SensorDistanciaUltra.jpg) |
| Piezo/Buzzer | ![](Piezo.jpg) |
| 220 Ohms | ![](Resistor220k.jpg) |
| 10k Ohms | ![](Resistor10k.jpg) |


## 4. Esquema Elétrico


O esquema elétrico pode ser visto por:

![](esquema.png)

## 5. Código

```Cpp
#include <LiquidCrystal.h>
#define TEMPO_ATUALIZACAO 20
#define ECHO 7 
#define TRIG 8
#define buzzer 6
#define freq  1000
int maximo = 300;//distancia máxima: 300cm
int minimo = 5;//distância mínima: 5cm
float duracao  = 0 ;
float  distcm = 0;
float velocidade = 0.0172316;

//LCD
LiquidCrystal lcd(12,11,5,4,3,2);

byte carroPart_1[8] = {0b00000,0b00000,0b00001,0b00011,0b00111,0b01111,0b11111,0b11111};
byte carroPart_2[8] = {0b00000,0b00000,0b10001,0b11111,0b11111,0b00110,0b00110,0b00110};
byte carroPart_3[8] = {0b00000,0b00000,0b10000,0b11000,0b11100,0b11100,0b11100,0b11100};
byte carroPart_4[8] = {0b11111,0b11111,0b01111,0b00111,0b00011,0b00001,0b00000,0b00000};
byte carroPart_5[8] = {0b00110,0b00110,0b00110,0b11111,0b11111,0b10001,0b00000,0b00000};
byte carroPart_6[8] = {0b11100,0b11100,0b11100,0b11100,0b11000,0b10000,0b00000,0b00000};
byte parede_grande[8]={0b11111,0b11111,0b11111,0b11111,0b11111,0b11111,0b11111,0b11111};

void setup()
{
 pinMode(12,OUTPUT);
 pinMode(11,OUTPUT);
 lcd.begin(16,2);
 pinMode(TRIG,OUTPUT);
 pinMode(ECHO,INPUT);
 pinMode(buzzer,OUTPUT);
 lcd.begin(16, 2);
 lcd.createChar(1, carroPart_1);
 lcd.createChar(2, carroPart_2);
 lcd.createChar(3, carroPart_3);
 lcd.createChar(4, carroPart_4);
 lcd.createChar(5, carroPart_5);
 lcd.createChar(6, carroPart_6);
 lcd.createChar(7, parede_grande);
}

void loop()
{
  lcd.setCursor(0,0);
  //Envia um Pulso
  digitalWrite(TRIG,LOW);
  delayMicroseconds(5);
  digitalWrite(TRIG,HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG,LOW);
  duracao = pulseIn(ECHO,HIGH);
  
  //calcular a distancia em centímetro
  distcm = duracao * velocidade;
  //lcd.clear();
  //4 fase
  if(distcm <= 200 && distcm > 100)
  {
    //carro
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write(1);
    lcd.setCursor(1,0);
    lcd.write(2);
    lcd.setCursor(2,0);
    lcd.write(3);
    lcd.setCursor(0,3);
    lcd.write(4);
    lcd.setCursor(1,2);
    lcd.write(5);
    lcd.setCursor(2,3);
    lcd.write(6);
    // fim carro
    if(distcm <= 200 && distcm > 150  )
    {
    //parede 
    lcd.setCursor(15,0);
    lcd.write(7);
    lcd.setCursor(15,1);
    lcd.write(7);
    }
    else if(distcm <= 150 && distcm > 100  )
    {
    //parede 
    lcd.setCursor(14,0);
    lcd.write(7);
    lcd.setCursor(14,1);
    lcd.write(7);
    }
  }
  //3 fase
  if(distcm <= 100 && distcm > 50)
  {
    lcd.clear();
     //carro
    lcd.setCursor(0,0);
    lcd.write(1);
    lcd.setCursor(1,0);
    lcd.write(2);
    lcd.setCursor(2,0);
    lcd.write(3);
    lcd.setCursor(0,3);
    lcd.write(4);
    lcd.setCursor(1,2);
    lcd.write(5);
    lcd.setCursor(2,3);
    lcd.write(6);
    // fim carro
    if(distcm <= 100 && distcm > 70)
    {
    //parede 
    lcd.setCursor(13,0);
    lcd.write(7);
    lcd.setCursor(13,1);
    lcd.write(7);
    }
    if(distcm <= 70 && distcm > 50  )
    {
    //parede 
    lcd.setCursor(11,0);
    lcd.write(7);
    lcd.setCursor(11,1);
    lcd.write(7);
    //fim parede
    } 
  }
  //2 fase
  if(distcm <= 50 && distcm >= 30)
  {
    //carro
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write(1);
    lcd.setCursor(1,0);
    lcd.write(2);
    lcd.setCursor(2,0);
    lcd.write(3);
    lcd.setCursor(0,3);
    lcd.write(4);
    lcd.setCursor(1,2);
    lcd.write(5);
    lcd.setCursor(2,3);
    lcd.write(6);
    // fim carro
    if(distcm <= 50 && distcm > 42)
    {
    //parede 
    lcd.setCursor(9,0);
    lcd.write(7);
    lcd.setCursor(9,1);
    lcd.write(7);
    }
    if(distcm <= 42 && distcm >  35 )
    {
    //parede 
    lcd.setCursor(8,0);
    lcd.write(7);
    lcd.setCursor(8,1);
    lcd.write(7);
    }
    if(distcm <= 35 && distcm > 30  )
    {
    //parede 
    lcd.setCursor(7,0);
    lcd.write(7);
    lcd.setCursor(7,1);
    lcd.write(7);
    //fim parede 
    }
    tone(buzzer,freq);
    delay(500);
    noTone(buzzer);
    delay(500);
    
  }
  //1 fase
  if(distcm < 30 && distcm >= 5)
  {
    lcd.clear();
    //carro
    lcd.setCursor(0,0);
    lcd.write(1);
    lcd.setCursor(1,0);
    lcd.write(2);
    lcd.setCursor(2,0);
    lcd.write(3);
    lcd.setCursor(0,3);
    lcd.write(4);
    lcd.setCursor(1,2);
    lcd.write(5);
    lcd.setCursor(2,3);
    lcd.write(6);
    // fim carro
    if(distcm <= 30 && distcm > 18)
    {//parede 
    lcd.setCursor(6,0);
    lcd.write(7);
    lcd.setCursor(6,1);
    lcd.write(7);
    }
    if(distcm <= 18 && distcm >  12 )
    {
    //parede 
    lcd.setCursor(5,0);
    lcd.write(7);
    lcd.setCursor(5,1);
    lcd.write(7);
    }
    if(distcm <= 12 && distcm > 5  )
    {
    //parede 
    lcd.setCursor(4,0);
    lcd.write(7);
    lcd.setCursor(4,1);
    lcd.write(7);
    }//fim parede 
    tone(buzzer,freq);
    delay(500);
    noTone(buzzer);
    delay(250);
  }
  //Fase Terminal
  if (distcm < 5)
  {
    //carro
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write(1);
    lcd.setCursor(1,0);
    lcd.write(2);
    lcd.setCursor(2,0);
    lcd.write(3);
    lcd.setCursor(0,3);
    lcd.write(4);
    lcd.setCursor(1,2);
    lcd.write(5);
    lcd.setCursor(2,3);
    lcd.write(6);
    // fim carro
    //parede
    lcd.setCursor(3,0);
    lcd.write(7);
    lcd.setCursor(3,1);
    lcd.write(7);
    //fim parede 
    tone(buzzer,freq);
    
  }
  else if(distcm > 200)
  {
    lcd.clear();
    noTone(buzzer);
  }
  delay(TEMPO_ATUALIZACAO);
}
```

## 6. Resultados

O vídeo de demonstração pode ser visto 
<a href="https://www.youtube.com/watch?v=cLu4DzUojdw&feature=youtu.be">AQUI </a>
{{< youtube cLu4DzUojdw >}}

## 7. Desafios encontrados

* Trabalhar com branch.
* Criar o sprite do carro

## 8. Melhorias

* Adicionar mais sensores ultrassônicos
* Calcular a escala correta da movimentação do objeto no display

